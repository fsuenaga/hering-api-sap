<?php 

require_once( "./conn.php" ) ;
require_once( './nusoap/lib/nusoap.php' ) ; 

$namespace = "http://complianceqa.ciahering.com.br/hering/webservices/sap/from_SAP.php" ;
$server = new soap_server ;
$server->configureWSDL( 'updateFornecedor' ) ;
$server->wsdl->schemaTargetNamespace = $namespace ;
$server->register( 
    'updateFornecedor',
    array(
        'CDFOR ' => 'xsd:string',
        'CNPJ  ' => 'xsd:string',
        'RAZSOC' => 'xsd:string',
        'NOMFAN' => 'xsd:string',
        'LOGRAD' => 'xsd:string',
        'NUMERO' => 'xsd:string',
        'COMPLE' => 'xsd:string',
        'CEP   ' => 'xsd:string',
        'BAIRRO' => 'xsd:string',
        'CIDADE' => 'xsd:string',
        'ESTADO' => 'xsd:string',
        'DTCRI ' => 'xsd:string',
        'BLOQUE' => 'xsd:string',
        'FONE  ' => 'xsd:string',
        'EMAIL ' => 'xsd:string'
    ),
        array(
        'CDFOR ' => 'xsd:string',
        'CNPJ  ' => 'xsd:string',
        'RAZSOC' => 'xsd:string',
        'NOMFAN' => 'xsd:string',
        'LOGRAD' => 'xsd:string',
        'NUMERO' => 'xsd:string',
        'COMPLE' => 'xsd:string',
        'CEP   ' => 'xsd:string',
        'BAIRRO' => 'xsd:string',
        'CIDADE' => 'xsd:string',
        'ESTADO' => 'xsd:string',
        'DTCRI ' => 'xsd:string',
        'BLOQUE' => 'xsd:string',
        'FONE  ' => 'xsd:string',
        'EMAIL ' => 'xsd:string'
    ),
    'http://complianceqa.ciahering.com.br/hering/webservices/sap/from_SAP.php',
    false, 
    'document'
) ;
$server->service( isset( $HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '' ) ;

function updateFornecedor( $CDFOR, $CNPJ, $RAZSOC, $NOMFAN, $LOGRAD, $NUMERO, $COMPLE, $CEP, $BAIRRO, $CIDADE, $ESTADO, $DTCRI, $BLOQUE, $FONE, $EMAIL ) {
    
    $originalPath = explode( '\\', $_SERVER['PATH_TRANSLATED'] ) ;
    array_pop( $originalPath );
    $path = implode( '\\', $originalPath ) ;

    $now = new DateTime ;
    $sNow = $now->format( "d-m-Y H:i:s" ) ;
    $sDay = $now->format( "Ymd" ) ;
    $fileName = $path . ".\\log\\updateFornecedor" . $sDay . ".log" ;

    $logFile = fopen( $fileName, "a+" ) or die( "Impossível abrir o arquivo de log: " . $fileName ) ;    
    fwrite( $logFile, "\r" ) ;
    fwrite( $logFile, "\r---------------------------------------" ) ;
    fwrite( $logFile, "\r" ) ;
    fwrite( $logFile, "--- update do SAP - " . $sNow ) ;
    fwrite( $logFile, "\r" ) ;
    fwrite( $logFile, "---------------------------------------" ) ;
    
    $sqlBuscaFornecedor = "
        SELECT
	        cc_fornecedor.*, cc_fr.*
        FROM
	        cc_fornecedor
        INNER JOIN cc_fr ON cc_fr.fornecedor = cc_fornecedor.fornecedor
        WHERE
	        cc_fornecedor.CNPJ = '{$CNPJ}'
        -- AND cc_fr.ativo = 1
        AND cc_fr.empresa = 'hering'
        LIMIT 1 ;
    " ;

    $resBuscaFornecedor = mysql_query( $sqlBuscaFornecedor ) or die( mysql_error()  . ":" . $sqlBuscaFornecedor ) ;
	if( mysql_num_rows( $resBuscaFornecedor ) > 0 ) { 
		$regBuscaFornecedor = mysql_fetch_assoc( $resBuscaFornecedor ) ;
	    mysql_free_result( $resBuscaFornecedor ) ;
        $DTCRI = substr( $DTCRI, 0, 4 ) . "-" . substr( $DTCRI, 4, 2 ) . "-" . substr( $DTCRI, 6, 2 ) . " 00:00:00" ;
        
        if( $regBuscaFornecedor['nsap'] != $CDFOR ) {        
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET nsap = '{$CDFOR}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;

            fwrite( $logFile, "\rInformação de 'nsap' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['nsap'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$CDFOR}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['razaoSocial'] != $RAZSOC ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET razaoSocial = '{$RAZSOC}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'razaoSocial' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['razaoSocial'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$RAZSOC}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['razaoSocial'] != $NOMFAN ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET nomeFantasia = '{$NOMFAN}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'nomeFantasia' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['nomeFantasia'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$NOMFAN}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }        

        if( $regBuscaFornecedor['rua'] != $LOGRAD ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET rua = '{$LOGRAD}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'rua' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['rua'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$LOGRAD}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['numero'] != $NUMERO ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET numero = '{$NUMERO}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'numero' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['numero'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$NUMERO}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['complemento'] != $COMPLE ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET complemento = '{$COMPLE}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'complemento' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['complemento'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$COMPLE}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['cep9'] != $CEP ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET cep9 = '{$CEP}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'cep9' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['cep9'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$CEP}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['bairro'] != $BAIRRO ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET bairro = '{$BAIRRO}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'bairro' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['bairro'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$BAIRRO}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['cidade'] != $CIDADE ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET cidade = '{$CIDADE}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'cidade' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['cidade'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$CIDADE}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['estado'] != $ESTADO ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET estado = '{$ESTADO}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'estado' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['estado'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$ESTADO}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['resp_telefone'] != $FONE ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET resp_telefone = '{$FONE}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'resp_telefone' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['resp_telefone'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$FONE}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['resp_email'] != $EMAIL ) {
            $sqlUpdateFornecedor = "UPDATE cc_fornecedor SET resp_email = '{$EMAIL}' WHERE CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'resp_email' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['resp_email'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$EMAIL}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        if( $regBuscaFornecedor['criado'] != $DTCRI ) {
            $sqlUpdateFornecedor = "UPDATE cc_fr INNER JOIN cc_fornecedor ON cc_fr.fornecedor = cc_fornecedor.fornecedor SET criado = '{$DTCRI}' WHERE cc_fornecedor.CNPJ = '{$CNPJ}' ;" ;
            mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
            fwrite( $logFile, "\rInformação de 'criado' modificada...\r" ) ;
            fwrite( $logFile, "  de: " ) ;
            fwrite( $logFile, $regBuscaFornecedor['criado'] ) ;
            fwrite( $logFile, "\rpara: " ) ;
            fwrite( $logFile, "{$DTCRI}" ) ;
            fwrite( $logFile, "\r" ) ;	
        }
        
        switch ( $BLOQUE ) {
            case 0:
                $sqlUpdateFornecedor = "UPDATE cc_fr INNER JOIN cc_fornecedor ON cc_fr.fornecedor = cc_fornecedor.fornecedor SET ativo = '1', status_certificacao = '1' WHERE cc_fornecedor.CNPJ = '{$CNPJ}' ;" ;
                $ativo = "1 - ativo e desbloqueado";
                break ;
            case 1:
                $sqlUpdateFornecedor = "UPDATE cc_fr INNER JOIN cc_fornecedor ON cc_fr.fornecedor = cc_fornecedor.fornecedor SET ativo = '1', status_certificacao = '0' WHERE cc_fornecedor.CNPJ = '{$CNPJ}' ;" ;
                $ativo = "1 - ativo e bloqueado";
                break ;
            case 2:
                $sqlUpdateFornecedor = "UPDATE cc_fr INNER JOIN cc_fornecedor ON cc_fr.fornecedor = cc_fornecedor.fornecedor SET ativo = '0', status_certificacao = '0' WHERE cc_fornecedor.CNPJ = '{$CNPJ}' ;" ;
                $ativo = "0 - inativo" ;
                break ;
        }
        mysql_query( $sqlUpdateFornecedor ) or die( mysql_error()  . ":" . $sqlUpdateFornecedor ) ;
        fwrite( $logFile, "\rInformação de 'ativo' modificada...\r" ) ;
        fwrite( $logFile, "\rpara: " ) ;
        fwrite( $logFile, "{$ativo}" ) ;
        
	} else { 
        $sqlInsereFornecedor = "
            INSERT INTO cc_fornecedor
            SET fornecedor = '{$CNPJ}',
             razaoSocial = '{$RAZSOC}',
             nomeFantasia = '{$NOMFAN}',
             categoria = '2',
             sub = '4',
             CNPJ = '{$CNPJ}',
             segmento = NULL,
             classe = '4',
             resp_telefone = '{$FONE}',
             resp_celular = '{$FONE}',
             resp_email = '{$EMAIL}',
             perfil = '1',
             rua = '{$LOGRAD}',
             numero = '{$NUMERO}',
             complemento = '{$COMPLE}',
             bairro = '{$BAIRRO}',
             cidade = '{$CIDADE}',
             estado = '{$ESTADO}',
             pais = 'BRASIL',
             cep9 = '{$CEP}',
             tipo_documento = 'CNPJ',
             telefonecontato = '{$FONE}',
             emailcontato = '{$EMAIL}'
        ";
        mysql_query( $sqlInsereFornecedor ) or die( mysql_error()  . ":" . $sqlInsereFornecedor ) ;        
        fwrite( $logFile, "\rCriado em cc_fornecedor:\r" ) ;
        fwrite( $logFile, $sqlInsereFornecedor ) ;
        fwrite( $logFile, "\r" ) ;	
       
        $sqlInsereFr = "
            INSERT INTO cc_fr
            SET empresa = 'hering',
             fornecedor = '{$CNPJ}',
             classe = '1',
             pAcesso = '0',
             passo_contrato = '3',
             data_passo_contrato = DATE(NOW()),
             passo_sistema = '5',
             certificadora = 'hering',
             ativo = '1',
             criado = NOW()
		" ;
        mysql_query( $sqlInsereFr ) or die( mysql_error()  . ":" . $sqlInsereFr ) ;
        fwrite( $logFile, "\rCriado em cc_fr:\r" ) ;
        fwrite( $logFile, $sqlInsereFr ) ;
        fwrite( $logFile, "\r" ) ;	
	}
    
    fwrite( $logFile, "\r" ) ;	
    fclose( $logFile ) ;
    return ;
}
