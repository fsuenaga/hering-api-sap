<?php 
header('Content-Type: text/html; charset=iso-8859-1');
error_reporting( E_ERROR | E_PARSE ) ;

echo "<pre>" ;

require_once( "./conn.php" ) ;
require_once( "./nusoap/lib/nusoap.php" ) ; 

$sql = "
SELECT
	cc_fornecedor.nsap AS CODFOR,
	cc_fornecedor.CNPJ AS CNPJ,
	cc_fornecedor.razaoSocial AS RAZSOC,
	cc_fornecedor.nomeFantasia AS NOMFAN,
	cc_fornecedor.rua AS LOGRAD,
	cc_fornecedor.numero AS NUMERO,
	cc_fornecedor.complemento AS COMPLE,
	cc_fornecedor.cep9 AS CEP,
	cc_fornecedor.bairro AS BAIRRO,
	cc_fornecedor.cidade AS CIDADE,
	cc_fornecedor.estado AS ESTADO,
	cc_fr.criado AS DTCRI,
	CASE cc_fr.ativo
WHEN 1 THEN

IF (
	cc_fr.status_certificacao = 1,
	0,
	1
)
WHEN 0 THEN
	2
END AS BLOQUE,
 cc_fornecedor.resp_telefone AS FONE,
 cc_fornecedor.resp_email AS EMAIL
FROM
	cc_fr
INNER JOIN cc_fornecedor ON cc_fr.fornecedor = cc_fornecedor.fornecedor
WHERE
	cc_fornecedor.nsap IS NOT NULL
AND cc_fornecedor.sub IN (1, 2, 3, 4, 6, 8, 9)
AND cc_fr.status_certificacao IS NOT NULL
;" ;

$res_sql = mysql_query( $sql ) or die( mysql_error . ": " . $sql ) ;

$client = new SoapClient( "./QA_SOAP_COMPLIANCE_SiFornOut_Async_SOAP_COMPLIANCE.wsdl", array( "trace" => true ) ) ;
$client->setCredentials( "compluser", "complianceuser" ) ;

$operation = "SiFornOut_Async" ;

while( $row_sql = mysql_fetch_assoc( $res_sql ) ) {
    $CODFOR = strlen( $row_sql['CODFOR'] ) > 10 ? substr( $row_sql['CODFOR'], 0, 10 ) : $row_sql['CODFOR'] ;
    
    $CNPJ   = strlen( $row_sql['CNPJ'] ) > 16 ? substr( $row_sql['CNPJ'], 0, 16 ) : $row_sql['CNPJ'] ;   
    
    $RAZSOC = strlen( $row_sql['RAZSOC'] ) > 40 ? substr( $row_sql['RAZSOC'], 0, 40 ) : $row_sql['RAZSOC'] ; 
    
    $NOMFAN = strlen( $row_sql['NOMFAN'] ) > 40 ? substr( $row_sql['NOMFAN'], 0, 40 ) : $row_sql['NOMFAN'] ; 
    
    $LOGRAD = strlen( $row_sql['LOGRAD'] ) > 60 ? substr( $row_sql['LOGRAD'], 0, 60 ) : $row_sql['LOGRAD'] ; 
    
    $NUMERO = strlen( $row_sql['NUMERO'] ) > 10 ? substr( $row_sql['NUMERO'], 0, 10 ) : $row_sql['NUMERO'] ; 
    
    $COMPLE = strlen( $row_sql['COMPLE'] ) > 10 ? substr( $row_sql['COMPLE'], 0, 10 ) : $row_sql['COMPLE'] ; 
    
    $CEP    = strlen( $row_sql['CEP'] ) > 10 ? substr( $row_sql['CEP'], 0, 10 ) : $row_sql['CEP'] ;
    $CEP    = str_replace( "-", "", $CEP ) ;
    
    $BAIRRO = strlen( $row_sql['BAIRRO'] ) > 40 ? substr( $row_sql['BAIRRO'], 0, 40 ) : $row_sql['BAIRRO'] ; 
    
    $CIDADE = strlen( $row_sql['CIDADE'] ) > 40 ? substr( $row_sql['CIDADE'], 0, 40 ) : $row_sql['CIDADE'] ; 
    
    $ESTADO = strlen( $row_sql['ESTADO'] ) > 3 ? substr( $row_sql['ESTADO'], 0, 3 ) : $row_sql['ESTADO'] ; 
    
    $DTCRI  = substr( $row_sql['DTCRI'], 0, 4 ) . substr( $row_sql['DTCRI'], 5, 2 ) . substr( $row_sql['DTCRI'], 8, 2 ) ;
    
    $BLOQUE = strlen( $row_sql['BLOQUE'] ) > 1 ? substr( $row_sql['BLOQUE'], 0, 1 ) : $row_sql['BLOQUE'] ; 
    
    $FONE   = strlen( $row_sql['FONE'] ) > 30 ? substr( $row_sql['FONE'], 0, 30 ) : $row_sql['FONE'] ;   
    
    $EMAIL  = strlen( $row_sql['EMAIL'] ) > 80 ? substr( $row_sql['EMAIL'], 0, 80 ) : $row_sql['EMAIL'] ;  
    $aEmails = explode( ";", $EMAIL ) ;
    $EMAIL = $aEmails[0] ;
    if( $EMAIL == null ) {
        $EMAIL = 'NAO INFORMADO' ;
    }

    $params = array( "mtForn_out" =>
                    array( "DADOSFORN" => 
                        array(
                            "CDFOR" => "{$CODFOR}",
                            "CNPJ" => "{$CNPJ}",
                            "RAZSOC" => "{$RAZSOC}",
                            "NOMFAN" => "{$NOMFAN}",
                            "LOGRAD" => "{$LOGRAD}",
                            "NUMERO" => "{$NUMERO}",
                            "COMPLE" => "{$COMPLE}",
                            "CEP" => "{$CEP}",
                            "BAIRRO" => "{$BAIRRO}",
                            "CIDADE" => "{$CIDADE}",
                            "ESTADO" => "{$ESTADO}",
                            "DTCRI" => "{$DTCRI}",
                            "BLOQUE" => "{$BLOQUE}",
                            "FONE" => "{$FONE}",
                            "EMAIL" => "{$EMAIL}"
                        ) 
                    ) 
                ) ;
    try
    {
        //print_r( $params['mtForn_out']['DADOSFORN']['CDFOR'] ) ; echo "\r" ;
        $client->call( $operation, $params, "", "", false, null, "document", "encoded" ) ;
    }
    catch( SoapFault $exception )
    {
        print "***Caught Exception***\n" ;
        print_r( $exception );
        print "***END Exception***\n" ;
        die() ;
    }    
    
    // echo '<h2>Request</h2>';
    // echo htmlspecialchars( $client->request, ENT_QUOTES) ;

    //echo '<h2>Response</h2>';
    //echo htmlspecialchars( $client->response, ENT_QUOTES) ;

    // Display the debug messages
    //echo '<h2>Debug</h2>';
    //echo htmlspecialchars( $client->debug_str, ENT_QUOTES) ;

}
mysql_free_result( $res_sql ) ;

