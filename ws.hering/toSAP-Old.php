<?php 

error_reporting( E_ERROR | E_PARSE ) ;

echo "<pre>" ;

require_once( "./conn.php" ) ;
require_once( "./nusoap/lib/nusoap.php" ) ; 

$client = new SoapClient( "./QA_SOAP_COMPLIANCE_SiFornOut_Async_SOAP_COMPLIANCE.wsdl", array( "trace" => true ) ) ;
$client->setCredentials( "compluser", "complianceuser" ) ;

$operation = "SiFornOut_Async" ;

$params = array( "mtForn_out" =>
                array( "DADOSFORN" => 
                    array(
                        "CDFOR" => "139506",
                        "CNPJ" => "14155753000180",
                        "RAZSOC" => "A MARIA DOS SANTOS SILVA FACCAO ",
                        "NOMFAN" => "A MARIA DOS SANTOS SILVA FACCAO ",
                        "LOGRAD" => "Travessa Julio Cassiano de Araujo, 271",
                        "NUMERO" => "",
                        "COMPLE" => "",
                        "CEP" => "59370-000",
                        "BAIRRO" => "CENTRO",
                        "CIDADE" => "ACARI",
                        "ESTADO" => "RN",
                        "DTCRI" => "",
                        "BLOQUE" => "1",
                        "FONE" => "(84)9999-8984",
                        "EMAIL" => ""
                    ) 
                ) 
            ) ;

//$options = array( "location" => "http://bnsvher309.hering.local:50000/XISOAPAdapter/MessageServlet?channel=:SOAP_COMPLIANCE:CC_SoapSndFornecedorComplianceSAP" ) ;

try
{
    $client->call( $operation, $params, "", "", false, null, "document", "encoded" ) ;
}
catch( SoapFault $exception )
{
    print "***Caught Exception***\n" ;
    print_r( $exception );
    print "***END Exception***\n" ;
    die() ;
}

echo '<h2>Request</h2>';
echo htmlspecialchars( $client->request, ENT_QUOTES) ;

echo '<h2>Response</h2>';
echo htmlspecialchars( $client->response, ENT_QUOTES) ;

// Display the debug messages
//echo '<h2>Debug</h2>';
//echo htmlspecialchars( $client->debug_str, ENT_QUOTES) ;
